#include <stdio.h>
#include <string.h>
#include <malloc.h>


struct User{
    
    char username[64];
    char password[200];
    
    char email[35];
    
};



int main(){
    
    struct User u1;
    struct User * u2 = (struct User *)malloc(sizeof(struct User));
    struct User u3;
    
    strcpy(u1.email, "john@gmail.com");
    strcpy(u2->email,"khgaldrogo@got.ro"); //(*u2).email
    strcpy(u3.email, "bill@gates.us");
    
    struct User utilizatori[3];
    utilizatori[0] = u1;
    utilizatori[1] = *u2;
    utilizatori[2] = u3;
    
    int i;
    for(i=0; i<3; i++){
        printf("\t\tEmail: %s\n ", utilizatori[i].email);
    }
    
    printf("======================================================\n");
    
    struct User * utilizatori2[3];
    
    utilizatori2[0] = &u1;
    utilizatori2[1] = u2;
    utilizatori2[2] = &u3;
    
    for(i=0; i<3; i++){
        printf("\tEmail: %s\n", (*utilizatori2[i]).email);
        // printf("\tEmail: %s\n", utilizatori2[i]->email);
    }
    
    return 0;
}