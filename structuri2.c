#include <stdio.h>
#include <string.h>
#include <malloc.h>

#define EMAIL_MAX_SIZE 30

struct User{
    
    char username[64];
    char password[200];
    
    // char * password;
    
    char email[35]; // 1 singur
    // 5 email-uri
    char * emails[5]; // char ** emails;
    int nrEmailuri;
};

int main(){
    
    struct User u1;
    
    strcpy(u1.username, "jim");
    
    u1.emails[0] = (char*)malloc(sizeof(char)* EMAIL_MAX_SIZE);
    strcpy(u1.emails[0], "jim@gmail.com");
    
    u1.emails[1] = (char*)malloc(sizeof(char)* EMAIL_MAX_SIZE);
    strcpy(u1.emails[1], "jim@wilde.com");
    
    int i;
    for(i=0; i<2; i++){
        printf("Email: %s\n", u1.emails[i]);
    }
    
    u1.nrEmailuri = 2;
    
    struct User u2;
    
    strcpy(u2.username, "bob");
    
    u2.emails[0] = (char*)malloc(sizeof(char)* EMAIL_MAX_SIZE);
    strcpy(u2.emails[0], "bob@bob.bob");
    
    u2.nrEmailuri = 1;
    
    struct User utilizatoriiToti[2];
    utilizatoriiToti[0] = u1;
    utilizatoriiToti[1] = u2;
    
    
    
    // toti utilizatorii (numele si email-urile asociate)
    // jim
    //      jim@gmail.com
    //      jim@wilde.com
    // bob
    //      bob@bob.com
    // .........
    
    int i;
    for(i=0; i<2; i++){
        printf("%s\n", utilizatoriiToti[i].username);
        int j;
        for(j=0; j<utilizatoriiToti[i].nrEmailuri; j++){
            printf("%s\t",utilizatoriiToti[i].emails[j])        
            
        }
    }
    
    return 0;
}