#include <stdio.h>
#include <string.h>
#include <malloc.h>

#define EMAIL_MAX_SIZE 30

// meniuri
//  - nume
//  - mancaruri (n)
// mancaruri
//  - nume
//  - pret
//  - ingrediente
// ingredient
//  - nume 
//  - gust

struct Meniu{
    char  nume[50];
    struct Mancare *mancaruri[10];
    int nrMancaruri;

};

struct Mancare{
    char nume[50];
    double pret;
    struct Ingredient *ingrediente[10];
    int nrIngrediente;
};

struct Ingredient{
    char nume[50];
    char gust[50];
    
};


int main(){
    struct Meniu m1;
    struct Mancare f1;
    struct Mancare f2;
    f1.ingrediente[0] = (struct Ingredient*)malloc(sizeof(struct Ingredient));
    f1.ingrediente[1] = (struct Ingredient*)malloc(sizeof(struct Ingredient));
    f1.ingrediente[2] = (struct Ingredient*)malloc(sizeof(struct Ingredient));
    
    f2.ingrediente[0] = (struct Ingredient*)malloc(sizeof(struct Ingredient));
    f2.ingrediente[1] = (struct Ingredient*)malloc(sizeof(struct Ingredient));
    m1.mancaruri[0]=&f1;
    m1.mancaruri[1]=&f2;
    m1.nrMancaruri=2;
    f1.nrIngrediente=3;
    f2.nrIngrediente=2;

    struct Meniu m2;
    struct Mancare f3;
    f3.ingrediente[0] = (struct Ingredient*)malloc(sizeof(struct Ingredient));
    m2.mancaruri[0]=&f3;
    m2.nrMancaruri=1;
    f3.nrIngrediente=1;
    
    struct Meniu nrMeniuri[2];
    nrMeniuri[0]=m1;
    nrMeniuri[1]=m2;
    
    
    int i,j,k;
    for(i=0;i<2;i++){
        printf("\t\tMeniul %s\n",nrMeniuri[i].nume);
            for(j=0;j<nrMeniuri[i].nrMancaruri;j++){
                
                printf("\tFELUL:%s     PRET:%lf\n ",nrMeniuri[i].mancaruri[j]->nume,nrMeniuri[i].mancaruri[j]->pret);
                    for(k=0;k<nrMeniuri[i].mancaruri[j]->nrIngrediente;k++){
                        printf("Nume Ingredient: %s       GUST: %s \n",nrMeniuri[i].mancaruri[j]->ingrediente[k]->nume,nrMeniuri[i].mancaruri[j]->ingrediente[k]->gust);
                    }
            }
        
    }
    // fiecare meniu
    //     - mancare
    //          - ingredient
    // ....
    //     - mancare
    //  meniu 
    //     - mancare
    //          - ingredient
    return 0;
    
}