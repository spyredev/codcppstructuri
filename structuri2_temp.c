#include <stdio.h>
#include <string.h>
#include <malloc.h>

#define EMAIL_MAX_SIZE 30

struct User{
    
    char username[64];
    char password[200];
    
    char email[35]; // 1 singur
    // 5 email-uri
    char * emails[5];
};

int main(){
    
    char * emails[5];
    emails[0] = (char*)malloc(sizeof(char)* EMAIL_MAX_SIZE);
    strcpy(emails[0], "jim@gmail.com");
    
    emails[1] = (char*)malloc(sizeof(char)* EMAIL_MAX_SIZE);
    strcpy(emails[1], "olivia@wilde.com");
    
    int i;
    for(i=0; i<2; i++){
        printf("Email: %s\n", *(emails+i));
    }
    
    return 0;
}